public class Product {
    /*
    Chciałbym, aby powstała klasa Product, w której wszystkie powyższe parametry będą polami tej klasy. Innymi słowy na produkt składa się coś,
co ma nazwę, cenę, typ, oraz wiemy z jakiej jest półki.
     */
   private  String nazwaProduktu;
   private  int cena;
    PRODUCT_TYPE type;
    PRODUCT_CLASS klasa;

    public Product(String nazwaProduktu, int cena, PRODUCT_TYPE type, PRODUCT_CLASS klasa) {
        this.nazwaProduktu = nazwaProduktu;
        this.cena = cena;
        this.type = type;
        this.klasa = klasa;
    }

    public String getNazwaProduktu() {
        return nazwaProduktu;
    }

    public void setNazwaProduktu(String nazwaProduktu) {
        this.nazwaProduktu = nazwaProduktu;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public PRODUCT_TYPE getType() {
        return type;
    }

    public void setType(PRODUCT_TYPE type) {
        this.type = type;
    }

    public PRODUCT_CLASS getKlasa() {
        return klasa;
    }

    public void setKlasa(PRODUCT_CLASS klasa) {
        this.klasa = klasa;
    }
}
