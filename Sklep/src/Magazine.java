import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Magazine {
    private Map<PRODUCT_CLASS, List<Product>> produktyZKlasa = new HashMap<>();

    public Magazine(){
        produktyZKlasa.put(PRODUCT_CLASS.LOW, new LinkedList<>());
        produktyZKlasa.put(PRODUCT_CLASS.MID, new LinkedList<>());
        produktyZKlasa.put(PRODUCT_CLASS.HIGH, new LinkedList<>());
        produktyZKlasa.put(PRODUCT_CLASS.LOW, new LinkedList<>());
    }

    public void addProduct(Product product) {
        produktyZKlasa.put(product.getKlasa(),product);
    }
}
